package io.gitlab.devraptors.external;

import com.mongodb.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Arrays;

public class MongoInteraction {
    public static final class MongoConfig {
        public final String host;
        public final int port;
        public final String db;
        public final String username;
        public final String password;

        public MongoConfig(String host, int port, String db, String username, String password) {
            this.host = host;
            this.port = port;
            this.db = db;
            this.username = username;
            this.password = password;
        }
    }


    public MongoCredential credential;
    public DB database;
    DBCollection user_collection;
    DBCollection tx_collection;
    public MongoClient client;
    public String TABLE_FOR_USERS = "USERS";
    public String TABLE_FOR_TRANSACTIONS = "TRANSACTIONS";

    public MongoInteraction(MongoConfig config) {
        credential = MongoCredential.createCredential(config.username, config.db, config.password.toCharArray());
        client = new MongoClient(new ServerAddress(config.host, config.port), Arrays.asList(credential));
        database = client.getDB(config.db);
        user_collection = database.getCollection(TABLE_FOR_USERS);
        tx_collection = database.getCollection(TABLE_FOR_TRANSACTIONS);
    }

    public void addTool(Long chat_id, String tool) {
        DBObject user_data = new BasicDBObject("_id", getNextSequence("userid", "counters"))
                .append("chat_id", chat_id)
                .append("tool", tool)
                .append("amount", 0)
                .append("balance", 1000000);
        user_collection.insert(user_data);
    }

    public DBCursor getFinanceForTools(Long chat_id) {
        BasicDBObject query = new BasicDBObject("chat_id", chat_id);
        return user_collection.find(query);
    }

    public void addNewTransaction(String data) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(data);
        System.out.println(jsonObject.get("chat_id"));
        DBObject transaction_data = new BasicDBObject("_id", getNextSequence("txid", "tx_counters"))
                .append("chat_id", jsonObject.get("chat_id"))
                .append("tool", jsonObject.get("tool"))
                .append("timestamp", jsonObject.get("timestamp"))
                .append("direction", jsonObject.get("direction"))
                .append("price", jsonObject.get("price"))
                .append("volume", jsonObject.get("volume"))
                .append("amount", jsonObject.get("amount"))
                .append("balance", jsonObject.get("balance"));
        tx_collection.insert(transaction_data);
    }

    public void updateValueOfAmountAndBalance(String data) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(data);

        BasicDBObject query = new BasicDBObject("chat_id", jsonObject.get("chat_id"));
        query.put("tool", jsonObject.get("tool"));
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("amount", jsonObject.get("amount"));
        updateFields.append("balance", jsonObject.get("balance"));
        BasicDBObject setQuery = new BasicDBObject();
        setQuery.append("$set", updateFields);
        user_collection.update(query, setQuery);
    }

    public DBCursor getAllTransactionWithTool(Long chat_id, String tool) {
        BasicDBObject query = new BasicDBObject("chat_id", chat_id);
        query.put("tool", tool);
        return tx_collection.find(query);
    }

    public String getNextSequence(String name, String col_name) {
        DBCollection collection = database.getCollection(col_name);
        BasicDBObject find = new BasicDBObject();
        find.put("_id", name);
        BasicDBObject update = new BasicDBObject();
        update.put("$inc", new BasicDBObject("seq", 1));
        DBObject obj = collection.findAndModify(find, update);
        return obj.get("seq").toString();
    }

}