package io.gitlab.devraptors.external;

import com.rabbitmq.client.*;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.function.UnaryOperator;
import java.util.logging.*;

public class Rabbit {

    public class RabbitCriticalException extends Exception {
        public RabbitCriticalException(String message) {
            super(message);
        }
    }

    public static  class RabbitConfig {
        public final String ip;
        public final int port;
        public final String user;
        public final String password;

        public RabbitConfig(String ip, int port, String user, String password) {
            this.ip = ip;
            this.port = port;
            this.user = user;
            this.password = password;
        }
    }

    public final RabbitConfig config;
    Connection connection;
    Channel channel;
    DeliverCallback consumer;
    Logger logger = Logger.getLogger(Rabbit.class.getName());

    public Rabbit(RabbitConfig config) throws RabbitCriticalException {
        this.config = config;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(this.config.user);
        factory.setPassword(this.config.password);
        factory.setHost(this.config.ip);
        factory.setPort(this.config.port);



        try {
            connection = factory.newConnection();
        } catch (Exception e) {
            logger.warning(e.getMessage());
            throw new RabbitCriticalException("RabbitMQ error. Can't create a connection.");
        }


        try {
            channel = connection.createChannel();
        } catch (Exception e) {
            logger.warning(e.getMessage());
            throw new RabbitCriticalException("RabbitMQ error. Can't create the consuming channel.");
        }

    }

    public void declareChannel(String name) throws RabbitCriticalException {
        try {
            channel.queueDeclare(name, false, false, false, null);
        } catch (Exception e) {
            logger.warning(e.getMessage());
            throw new RabbitCriticalException("RabbitMQ error. Can't start channel.");
        }
    }

    public void begin_consumption(MongoInteraction interaction, String channel_name) throws RabbitCriticalException{

        try {
            consumer = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                logger.info(" [x] Received " + message);
                try {
                    interaction.addNewTransaction(message);
                    interaction.updateValueOfAmountAndBalance(message);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

        } catch (Exception e) {
            logger.warning(e.getMessage());
            throw new RabbitCriticalException("RabbitMQ error. Exception during creating consumer");
        }

        try {
            channel.basicConsume(channel_name, true, consumer, consumerTag -> { });
        } catch (Exception e) {
            logger.warning(e.getMessage());
            throw new RabbitCriticalException("RabbitMQ error. Exception during consumption of tasks.");
        }
    }

    public void sendMessage(String message, String channel_name) throws IOException {
        channel.basicPublish("", channel_name, null, message.getBytes(StandardCharsets.UTF_8));
        logger.info(" [x] Sent " + message);
    }

}

