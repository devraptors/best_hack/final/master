package io.gitlab.devraptors;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import io.gitlab.devraptors.external.MongoInteraction;
import io.gitlab.devraptors.external.Rabbit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class ServerThread extends Thread {
    static MongoInteraction interaction;
    static Rabbit rabbit;

    public ServerThread(Rabbit rabbit, MongoInteraction interaction) {
       this.rabbit = rabbit;
       this.interaction = interaction;
    }

    @Override
    public void run() {
        try {
            rabbit.declareChannel("TASK_QUEUE");
        } catch (Rabbit.RabbitCriticalException e) {
            e.printStackTrace();
        }

        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress("localhost", Start.serverPort), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }


        configureEndpoints(server);

        server.start();

        try {
            rabbit.declareChannel("JOB_RESULT");
        } catch (Rabbit.RabbitCriticalException e) {
            e.printStackTrace();
        }

        try {
            rabbit.begin_consumption(interaction, "JOB_RESULT");
        } catch (Rabbit.RabbitCriticalException e) {
            e.printStackTrace();
        }
    }

    private static void configureEndpoints(HttpServer server) {
        server.createContext("/api/tools", exchange -> {
            if ("GET".equals(exchange.getRequestMethod())) {
                try {
                    URL obj = new URL(Start.marketUrl);
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");


                    InputStream request = con.getInputStream();
                    JSONObject data = Start.parseJsonFromRequest(request);

                    OutputStream os = exchange.getResponseBody();

                    exchange.sendResponseHeaders(200, data.toString().getBytes().length);
                    os.write(data.toString().getBytes(StandardCharsets.UTF_8));
                    os.flush();
                } catch (Exception e) {
                    System.out.println(e.toString());
                    exchange.sendResponseHeaders(500, -1);
                }
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        });

        server.createContext("/api/trade", (exchange -> {
            if ("POST".equals(exchange.getRequestMethod())) {
                try {
                    InputStream stream = exchange.getRequestBody();
                    JSONObject data = Start.parseJsonFromRequest(stream);
                    ArrayList<String> tools = (ArrayList<String>) data.get("tools");

                    for (int i = 0; i < tools.size(); i++) {
                        interaction.addTool((Long) data.get("chat_id"), tools.get(i));
                    }

                    rabbit.sendMessage(data.toString(), "TASK_QUEUE");

                    String result = "{'result':'success'}";
                    OutputStream os = exchange.getResponseBody();

                    exchange.sendResponseHeaders(200, result.getBytes().length);
                    os.write(result.getBytes(StandardCharsets.UTF_8));
                    os.flush();
                } catch (Exception e) {
                    System.out.println(e.toString());
                    exchange.sendResponseHeaders(500, -1);
                }
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        }));

        server.createContext("/api/finance", exchange -> {

            if("POST".equals(exchange.getRequestMethod())) {
                try {
                    InputStream stream = exchange.getRequestBody();
                    JSONObject data = Start.parseJsonFromRequest(stream);
                    DBCursor cursor = interaction.getFinanceForTools((Long) data.get("chat_id"));
                    ArrayList<JSONObject> tools = new ArrayList<>();
                    while (cursor.hasNext()) {
                        DBObject result = cursor.next();
                        JSONObject tool = new JSONObject();
                        tool.put("tool", result.get("tool"));
                        tool.put("amount", result.get("amount"));
                        tool.put("balance", result.get("balance"));
                        tools.add(tool);
                    }
                    JSONObject result = new JSONObject();
                    result.put("tools", tools);
                    OutputStream os = exchange.getResponseBody();

                    exchange.sendResponseHeaders(200, result.toString().getBytes().length);
                    os.write(result.toString().getBytes(StandardCharsets.UTF_8));
                    os.flush();
                } catch (Exception e) {
                    System.out.println(e.toString());
                    exchange.sendResponseHeaders(500, -1);
                }
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        });

        server.createContext("/api/dots", exchange -> {});

        server.createContext("/api/report", exchange -> {
            if("POST".equals(exchange.getRequestMethod())) {
                try {
                    InputStream stream = exchange.getRequestBody();
                    JSONObject data = Start.parseJsonFromRequest(stream);
                    DBCursor cursor = interaction.getAllTransactionWithTool((Long) data.get("chat_id"), data.get("tool").toString());
                    ArrayList<JSONObject> transactions = new ArrayList<>();
                    while (cursor.hasNext()) {
                        DBObject result = cursor.next();
                        JSONObject transaction = new JSONObject();
                        transaction.put("timestamp", result.get("timestamp"));
                        transaction.put("direction", result.get("direction"));
                        transaction.put("price", result.get("price"));
                        transaction.put("volume", result.get("volume"));
                        transaction.put("amount", result.get("amount"));
                        transaction.put("balance", result.get("balance"));
                        transactions.add(transaction);
                    }
                    JSONObject result = new JSONObject();
                    result.put("transactions", transactions);
                    OutputStream os = exchange.getResponseBody();

                    exchange.sendResponseHeaders(200, result.toString().getBytes().length);
                    os.write(result.toString().getBytes(StandardCharsets.UTF_8));
                    os.flush();
                } catch (Exception e) {
                    System.out.println(e.toString());
                    exchange.sendResponseHeaders(500, -1);
                }
            } else {
                exchange.sendResponseHeaders(405, -1);
            }
        });
    }
}


public class Start {
    static String marketUrl = "https://httpbin.org/ip";
    static Integer serverPort = 5000;

    public static void main(String[] args) throws Rabbit.RabbitCriticalException {
        MongoInteraction.MongoConfig mongoConfig = new MongoInteraction.MongoConfig("localhost",
                27017,
                "bot",
                "bot",
                "passw0rd");


        MongoInteraction interaction = new MongoInteraction(mongoConfig);

        Rabbit.RabbitConfig rabbitConfig = new Rabbit.RabbitConfig("localhost",
                5672 ,
                "guest",
                "guest");


                Rabbit rabbit = new Rabbit(rabbitConfig);

        ServerThread serverThread = new ServerThread(rabbit, interaction);
        serverThread.start();
    }

    public static JSONObject parseJsonFromRequest(InputStream stream) throws IOException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject)jsonParser.parse(new InputStreamReader(stream, "UTF-8"));
        } catch (ParseException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
